import akka.actor._
import akka.cluster.singleton.{ ClusterSingletonManager, ClusterSingletonManagerSettings }
import com.typesafe.config.ConfigFactory

import scalax.collection.Graph // or scalax.collection.mutable.Graph
import scalax.collection.GraphPredef._, scalax.collection.GraphEdge._

import mcgp._
import mcgp.messages._
import mcgp.cstructs._
import mcgp.commands._

object Main {
  def main(args: Array[String]): Unit = {
    val nodeName = args(0)
    val defaultConfig = ConfigFactory.load()

    val minNrOfNodes= defaultConfig.getConfig("mcgp").getInt("min-nr-of-nodes")

    val nodeConfig = defaultConfig.getConfig(s"mcgp.nodes.${nodeName}")
    val hostname = nodeConfig.getString("hostname")
    val port = nodeConfig.getString("port")

    println(s"Node ${nodeName} running on ${hostname}:${port}")

    val config = ConfigFactory.parseString(s"""
      akka.remote.netty.tcp {
        hostname = ${hostname}
        port = ${port}
      }
      akka.cluster.roles = [mcgp]
      akka.cluster.role {
        mcgp.min-nr-of-members = ${minNrOfNodes} 
      }

      mcgp.node-id = ${nodeName}
      """).withFallback(defaultConfig)

    val system = ActorSystem("MCGPSystem", config)
    val node = system.actorOf(Props[Replica], "node")
    val singleton = system.actorOf(ClusterSingletonManager.props(
      singletonProps = Props[MembershipManager],
      //singletonName = "active",
      terminationMessage = PoisonPill,
      //role = Some("mcgp")),
      settings = ClusterSingletonManagerSettings(system).withRole("mcgp")),
      name = "manager")

    node ! StartConsole
  }

  /*
  def main(args: Array[String]): Unit = {

    
    val c1 = new Cmd(1)
    val c2 = new Cmd(2)
    val c3 = new Cmd(3)
    val c4 = new Cmd(4)
    val c5 = new Cmd(5)
    val c6 = new Cmd(6)
    */

    /*
    val ch1: Chistory = Chistory()
    val ch2: Chistory = Chistory()

    ch1.append(c1)
    ch2.append(c1)
    println(ch1.areCompatible(ch1,ch2))
    */

    /*
    val ch1: Cstruct = Chistory()

    ch1.append(c1)
    ch1.append(c2)
    ch1.append(c3)
    ch1.append(c4)

    println(ch1)

    val ch2: Cstruct = Chistory()
    ch2.append(c1)
    ch2.append(c3)
    ch2.append(c2)
    ch2.append(c5)

    println(ch2)

    val ch3: Cstruct = Chistory()
    ch3.append(c1)
    ch3.append(c2)
    ch3.append(c4)

    println(ch3)

    val ch4: Cstruct = Chistory()
    ch4.append(c2)
    ch4.append(c3)
    ch4.append(c1)

    println(ch4)
    val ch5: Cstruct = Chistory()
    ch5.append(c1)
    ch5.append(c2)
    ch5.append(c3)
    println(ch5)


    val setTest = Set(ch4,ch5)
    val lubTest = setTest.head.lub(setTest.tail)

    println("Testando LUB com exemplo dissertacao lasaro")
    println(lubTest)
    

    val ch6: Cstruct = Chistory()
    ch6.append(c1)
    ch6.append(c2)
    ch6.append(c3)
    val ch7: Cstruct = Chistory()

    val ch8: Cstruct = Chistory()
    ch8.append(c1)
    ch8.append(c2)

    println("LUB tem que ter Graph CCI 1 2 e 3")

    val setTest2 = Set(ch6,ch7)
    val lubTest2 = ch8.lub(setTest2)
    println(lubTest2)
    

    val ch9: Cstruct = Chistory()
    val ch10: Cstruct = Chistory()
    ch9.append(c1)
    val ch11: Cstruct = Chistory()
    ch9.append(c1)

    val lubTest3 = ch9.lub(Set(ch10,ch11))
    println(lubTest3)
    */

    /*
    val ch1: Cstruct = Chistory()

    ch1.append(c1)
    ch1.append(c3)
    ch1.append(c4)
    ch1.append(c5)
    ch1.append(c6)

    println(ch1)

    val ch2: Cstruct = Chistory()

    ch2.append(c1)
    ch2.append(c3)
    ch2.append(c4)
    ch2.append(c6)

    println(ch2)

    val setTest = Set(ch1,ch2)
    val glbTest = setTest.head.glb(setTest.tail)

    println("Testando GLB")
    println(glbTest)


    val ch3: Cstruct = Chistory()
    ch3.append(c1)
    val ch4: Cstruct = Chistory()
    ch4.append(c3)
    val ch5: Cstruct = Chistory()
    ch5.append(c1)
    ch5.append(c3)
    val ch6: Cstruct = Chistory()
    ch6.append(c3)
    ch6.append(c1)
    ch6.append(c4)

    val setTest2 = Set(ch3,ch4,ch5,ch6)
    val glbTest2 = setTest2.head.glb(setTest2.tail)

    println("testando glb2")
    println(glbTest2)
    */
  //}
}
