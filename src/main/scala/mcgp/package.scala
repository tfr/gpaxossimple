import mcgp.cstructs._
import mcgp.commands._

package object mcgp {
  type Instance = Int
  type AgentId = String
  type Cmd = CommutativeCommandInt
  //type Cmd = SimpleCommandInt
  type CStruct = Chistory
  //type CStruct = Cseq
}
