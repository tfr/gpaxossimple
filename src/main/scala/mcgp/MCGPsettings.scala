package mcgp

import akka.actor.ActorSystem
import akka.actor.Extension
import akka.actor.ExtensionId
import akka.actor.ExtensionIdProvider
import akka.actor.ExtendedActorSystem
import akka.util.Helpers.Requiring
import akka.japi.Util.immutableSeq

//import scala.collection.immutable

import com.typesafe.config.Config
import com.typesafe.config.ConfigObject

class MCGPsettings(config: Config) extends Extension {

  // System configuration
  private val cc = config.getConfig("mcgp")

  val MinNrOfNodes: Int = {
    cc.getInt("min-nr-of-nodes")
  } requiring (_ > 0, "min-nr-of-nodes must be > 0")

  val RandomNrOfCmds: Int = {
    cc.getInt("random-nr-of-cmds")
  } requiring (_ > 0, "random-nr-of-cmds must be > 0")

  val totalNrOfAcceptors: Int = {
    cc.getInt("total-nr-of-acceptors")
  } requiring (_ > 0, "total-nr-of-acceptors must be > 0")

  val QuorumSize: Int = {
    cc.getInt("quorum-size")
  } requiring (_ > 0, "quorum-size must be > 0")

  val QuorumSizeFast: Int = {
    cc.getInt("quorum-size-fast")
  } requiring (_ > 0, "quorum-size-fast must be > 0")

  val DeliveryPolicy: String = cc.getString("delivery-policy")

  val MinNrOfAgentsOfRole: Map[String, Int] = {
    import scala.collection.JavaConverters._
    cc.getConfig("role").root.asScala.collect {
      case (key, value: ConfigObject) ⇒ (key -> value.toConfig.getInt("min-nr-of-agents"))
    }.toMap
  }

  val NodeId: String = cc.getString("node-id")

  // Node Configuration
  val nc: Config = cc.getConfig(s"nodes.${NodeId}")

  val NrOfAgentsOfRoleOnNode: Map[String, Int] = {
    import scala.collection.JavaConverters._
    nc.root.asScala.collect {
      case (key, value: ConfigObject) ⇒ (key -> value.toConfig.getInt("nr-of-agents"))
    }.toMap
  }

  val ProtocolRoles: Set[String] = immutableSeq(nc.getStringList("roles")).toSet
  
  // Agents Configuration
  val pc: Config = nc.getConfig("proposer")

  val lc: Config = nc.getConfig("learner")

  val ac: Config = nc.getConfig("acceptor")

  val ProposerIdsByName: Map[String, String] = {
    import scala.collection.JavaConverters._
    pc.root.asScala.collect {
      case (key, value: ConfigObject) ⇒ (key -> value.toConfig.getString("id"))
    }.toMap
  }

  val LearnerIdsByName: Map[String, String] = {
    import scala.collection.JavaConverters._
    lc.root.asScala.collect {
      case (key, value: ConfigObject) ⇒ (key -> value.toConfig.getString("id"))
    }.toMap
  }

  val AcceptorIdsByName: Map[String, String] = {
    import scala.collection.JavaConverters._
    ac.root.asScala.collect {
      case (key, value: ConfigObject) ⇒ (key -> value.toConfig.getString("id"))
    }.toMap
  }

}

object Settings extends ExtensionId[MCGPsettings] with ExtensionIdProvider {
  override def lookup = Settings

  override def createExtension(system: ExtendedActorSystem) = new MCGPsettings(system.settings.config)

   /*
    * Java API: retrieve the Settings extension for the given system.
    */
  override def get(system: ActorSystem): MCGPsettings = super.get(system)
}
