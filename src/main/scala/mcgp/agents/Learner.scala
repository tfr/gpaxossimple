package mcgp.agents

import mcgp._
import mcgp.messages._
import mcgp.protocol._

import scala.concurrent.ExecutionContext
import collection.concurrent.TrieMap

import akka.actor._

import scala.collection.mutable.{Set => MSet}
import scala.collection.mutable.{Map => MMap}

trait Learner extends ActorLogging {
  this: LearnerActor =>

  def learnerBehavior(config: ClusterConfiguration, state: LearnerMeta)(implicit ec: ExecutionContext): Receive = {
    case GetState =>
      log.info("ID: {} -- STATE: {}", id, state)

    case msg: UpdateConfig =>
      if(settings.totalNrOfAcceptors == msg.config.acceptors.size)
        listOfAcceptors = msg.config.acceptors.keys.toList
      context.become(learnerBehavior(msg.config, state))

    case msg: Msg2B =>
      log.info("REACHED LEARNERS")
      log.debug("REACHED LEARNERS")
      log.debug("ID: {} -- RECEIVED {} -- FROM {}", id, msg, msg.senderId)
      
      //Updating learning round
      if(msg.rnd > lrnd) {
        lrnd = msg.rnd
        quorum2bVotes = MMap[AgentId, CStruct]().withDefaultValue(bottomC)
      }

      //If we receive unordered votes from the acceptors we keep only the greatest extension.
      //We do this by checking if the new voted CStruct is the prefix of the previous one
      //If its not a prefix, then its 'bigger' and it should be added to the map of Votes.
      
      log.debug("LEARNER {} -- Tentando inserir valor {} na chave {}", id, msg.value.get, msg.senderId)
      if(!msg.value.get.isPrefixOf(quorum2bVotes(msg.senderId)))
        quorum2bVotes += (msg.senderId -> msg.value.get.asInstanceOf[CStruct])

      context.become(learnerBehavior(config, learn(msg, state, config)))
  }

  def glbQuorum(l: List[AgentId]): CStruct = {
    val toGLB = l.map(e =>  quorum2bVotes(e))
    val ret = (toGLB.head.glb(toGLB.tail.toSet)).asInstanceOf[CStruct]
    ret
  }

  def lubQuorum(l: List[AgentId]): Boolean = {
    val toLUB = l.map(e => quorum2bVotes(e))

    //gotta catch the exception
    val ret: Option[CStruct] = toLUB.head.lub(toLUB.tail.toSet).asInstanceOf[Option[CStruct]]
    if(ret == None) false else true
  }

  def checkCompatible(lt: List[(List[AgentId],Boolean)]) : Boolean = {
    lt.map(e => e._2).reduce((a,b) => a || b)
  }

  def learn(msg: Msg2B, state: LearnerMeta, config: ClusterConfiguration): LearnerMeta = {
   // val newLearned = state.learned.get.lub(Set(msg.value.get)).asInstanceOf[Option[CStruct]]
   // val newState = state.copy(learned = newLearned)
   // newState
    val qSize = if(msg.rnd.isClassic) settings.QuorumSize else settings.QuorumSizeFast

    //Each combination of acceptors agentIds is a possible Quorum.
    //If a quorum is deemed incompatible, the boolean is set to false. 
    //Such quorum wont become compatible anymore, therefore there is no need to operate over it again.
    //In the case of a newRound, gotta set all the booleans to true again,

    if (possibleQuorums.isEmpty)
      possibleQuorums = listOfAcceptors.combinations(qSize).toList.map(x => (x,true))
      //possibleQuorums = config.acceptors.keys.toList.combinations(qSize).toList.map(x => (x,true))

    //log.debug("LEARNER -- ID {} -- ACCEPTORS KEYS TO LIST {}",id, possibleQuorums)

    //log.debug("LEARNER ID: {} -- QUORUM2BVotes{}", id, quorum2bVotes)

    //log.debug("LEARNER -- ID {} QUORUM2BVOTES antes de entrar -- {} ", id, quorum2bVotes.size)
    if (quorum2bVotes.size >= qSize) {

      //log.debug("LEAERNER -- ID {} QUORUM2BVOTES ao entrar -- {} ", id, quorum2bVotes.size)
      log.debug("LEARNING PHASE - Learner {} - DID meet the quorum requirements with MSG ROUND: {} with state: {}", id, msg.rnd, state)

      //I will GLB only the compatible quorums, if Quorum is compatible, then I have the LUB of their Cstructs
      val onlyCompatible = possibleQuorums.map(f => if(lubQuorum(f._1)) (f._1,true) else (f._1,false))

      //log.debug("ONLYCOMPATIBLE TEM QUE ELIMINAR COMBINACOES{}", onlyCompatible)

      //Update possible Quorums
      possibleQuorums = onlyCompatible
     // log.debug("LEARNER ID -- {} -- POSSIBLE QUORUMS UPDATED {}",id, possibleQuorums)

      //Checking if there are compatible quorums
      if (checkCompatible(possibleQuorums)) { 

         //Find the GLB of the quorums that have compatible Cstructs
         val toBeLearned = possibleQuorums.filter(f => f._2 == true).map(g => glbQuorum(g._1))

         //log.debug("LEARNER ID -- {} toBeLearned {}", id, toBeLearned)

        // val toBeLearnedSet = toBeLearned.toSet
        // log.debug("LEARNER ID -- {} toBeLearnedSet {}", id, toBeLearnedSet)
        //
        // val estadoAtual = state.learned.get
        // log.debug("LEARNER ID -- {} ESTADO ATUAL {}", id,estadoAtual)

        // val tryingToLearn = estadoAtual.lub(toBeLearnedSet)
        // //val tryingToLearn = state.learned.get.lub(toBeLearnedSet)
        // log.debug("LEARNER ID -- {} state LUB trying to learn {}", id, tryingToLearn)
         
         //Its not necessary to LUB toBeLearned
         val newLearned = state.learned.get.lub(toBeLearned.toSet).asInstanceOf[Option[CStruct]]

         //log.debug("LEARNER ID -- {} newLearned {}", id, newLearned)
         //log.info("LEARNER ID -- {} newLearned {}", id, newLearned)

         //lembro disso daqui ser problematico, checar
         val newState = state.copy(learned = newLearned)

         //log.info("LEARNER ID -- {} com que estado estou lidando: {}", id, state)

         //log.info("LEARNER ID -- {} o estado para onde deveria ir: newState {}", id, newState)
         newState
      } else {
         log.debug("There is no quorum with compatible cstructs, asking leader to sort it out with new round")
         msg.rnd.coordinator.foreach(_ ! CollisionRecovery(msg.rnd))
         state
      }
    } else {
      log.debug("PHASE LEARN - Learner {} - DID NOT meet the quorum requirements with MSG ROUND: {} with state: {}", id, msg.rnd, state)
      state
    }
  }
}

class LearnerActor(val id: AgentId) extends Actor with Learner {
  val settings = Settings(context.system)
  
  val bottomC = new CStruct()
  var quorum2bVotes = MMap[AgentId, CStruct]().withDefaultValue(bottomC)

  var possibleQuorums = List[(List[AgentId],Boolean)]()

  var listOfAcceptors = List[AgentId]()

  var lrnd: Round = Round()

  override def preStart(): Unit = {
    log.info("Learner ID: {} UP on {}", id, self.path)
  }

  def receive = learnerBehavior(ClusterConfiguration(), LearnerMeta(Some(bottomC)))(context.system.dispatcher)
}

object LearnerActor {
  def props(id: AgentId) : Props = Props(classOf[LearnerActor], id)
}
