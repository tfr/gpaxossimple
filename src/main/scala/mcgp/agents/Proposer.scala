package mcgp.agents

import mcgp._
import mcgp.messages._
import mcgp.protocol._
import mcgp.commands._
import mcgp.cstructs._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.util.Random
import scala.concurrent.duration._
import scala.util.{Success, Failure}

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

import scala.collection.mutable.{Map => MMap}

trait Proposer extends ActorLogging {
  this: ProposerActor =>

  def checkAndUpdateRound(round: Round) = {
    log.info("ID: {} - My prnd: {} crnd: {} -- Updating to prnd and crnd: {}", id, prnd, crnd, round)
    if (prnd < round) {
      prnd = round
      grnd = round
    }
    // TODO check if it is coordinator
    if (crnd < round) {
      crnd = round
      grnd = round
    }
  }

  def getCRoundCount: Int = if(crnd < grnd) grnd.count + 1 else crnd.count + 1

  def proposerBehavior(config: ClusterConfiguration, state: ProposerMeta)(implicit ec: ExecutionContext): Receive = {
    case GetState =>
      log.info("ID: {} -- HAS STATE: {}", id, state)

    case CommandProposal(cmd) =>
      log.info("Received proposal: {} from {}", cmd, sender)
      //state.cval == None, means that phase2Start wasn't even reached yet.
      //this seems wrong
     // if(state.cval == None) {
     //   log.debug("STATE.CVAL == NONE")
     //   context.become(proposerBehavior(config, stashCommandState(config, state, cmd)))
     // } else {
      if (coordinators.nonEmpty) {
        log.debug("coordinators is nonEmpty {}", prnd)
        var round = prnd
        if (grnd > prnd) {
          log.debug("Proposer:{} - PRND: {} is less than GRND: {}", id, prnd, grnd)
          round = grnd
        }

        var thisQuorum = if (round.isClassic) settings.QuorumSize else settings.QuorumSizeFast

        if (config.acceptors.size >= thisQuorum) {
          log.debug("ACCEPTORS SIZE DURING COMMANDPROPOSAL: {}", config.acceptors.size)
          if (round.isClassic) {
            if (isCoordinatorOf(round)) {
              val proposalMsg = Proposal(id, round, Some(cmd))
              log.debug("Received a Command Proposal: {}, AND IM THE COORDINATOR OF ROUND {}, proposing...", cmd, round)  
              context.become(proposerBehavior(config, propose(proposalMsg, state, config)))
            } else {
              //log.warning("Received a Command Proposal: {}, BUT IM NOT COORDINATOR OF ROUND {}, stashing...", cmd, round)  
              log.warning("Received a Command Proposal: {}, BUT IM NOT COORDINATOR OF ROUND {}, forwarding to coordinator...", cmd, round)  
              val forwarding = round.coordinator.head
              forwarding ! CommandProposal(cmd)
              context.become(proposerBehavior(config, state))
              //context.become(proposerBehavior(config, stashCommandState(config, state, cmd)))
            }
          } else {
            val acceptorsSet = config.acceptors.values.toSet
            
            /*
            val newCmd = new Cmd(1)
            val newCmd2 = new Cmd(2)
            val a1 = config.acceptors.values.toSet.tail.head
            a1 ! Proposal(id, round, Some(newCmd))
            */

            acceptorsSet.foreach(_ ! Proposal(id, round, Some(cmd)))
            
            /*
            val a2 = config.acceptors.values.toSet.head

            a2 ! Proposal(id, round, Some(newCmd2))
            */

            /*
            val thisAcceptors = config.acceptors.values.toSet.head
            val thisAcceptors2 = config.acceptors.values.toSet.tail.head

            val thisAcceptors3 = config.acceptors.values.toSet.last
            
            //simulating a collison for debugging purposes
            val newCmd3 = new Cmd(3)

            thisAcceptors ! Proposal(id, round, Some(newCmd))

            //acceptorsSet.foreach(_ ! Proposal(id, round, Some(cmd)))
            acceptorsSet ! Proposal(id, round, Some(cmd))


            thisAcceptors2 ! Proposal(id, round, Some(newCmd2))

            thisAcceptors3 ! Proposal(id, round, Some(newCmd3))
            */
            context.become(proposerBehavior(config, state))
            log.debug("Received a Command Proposal: {}, FAST ROUND {}, FORWARDING PROPOSAL TO ACCEPTORS", cmd, round)  
          }
        } else {
          log.debug("Received a Command Proposal Message, but still not enough acceptors: [{}]. Stashing command...", config.acceptors.size)
          context.become(proposerBehavior(config, stashCommandState(config, state, cmd)))
        }
      } else {
        //TODO: roll back to stashCommandState, and limit stash size
        log.error("Coordinator NOT FOUND for round {}...", prnd)
        context.become(proposerBehavior(config, stashCommandState(config, state, cmd)))
      }
     // }

    //TODO: put the following 3 cases inside a function
   
    case msg: NewLeader =>
      coordinators = msg.coordinators
      //if(config.acceptors.size == waitFor) {
      //  log.info("Discovered the minimum of {} acceptors, starting protocol instance.", waitFor)
      if (msg.coordinators contains self) {
        log.debug("I'm a LEADER! My id is: {} - HASHCODE: {}", id, self.hashCode)
        log.info("I'm a LEADER! My id is: {} - HASHCODE: {}", id, self.hashCode)
        val rnd = Round(getCRoundCount, msg.coordinators, true)
        val configMsg = Configure(id, rnd)
        log.debug("{} Configuring using ROUND: {}", id, rnd)

        val proposersSet = config.proposers.values.toSet
        proposersSet.foreach(_ ! RoundUpdate(rnd))

        context.become(proposerBehavior(config, phase1A(configMsg, state, config)))
      } else {
        log.debug("I'm NOT the LEADER! My id is {} - {}", id, self)
        //TODO: forward pval commands to leader
        log.info("I'm NOT the LEADER! My id is {} - {}", id, self)
      }
    //  } else {
    //    log.debug("Up to {} acceptors, still waiting in Init until {} acceptors discovered.", config.acceptors.size, waitFor)
    //  }

    case FastRound =>
      log.debug("TRYING TO RECONFIGURE USING A FAST ROUND")
      if (coordinators contains self) {
        log.debug("I'm a LEADER! My id is: {} - HASHCODE: {}", id, self.hashCode)
        log.info("I'm a LEADER! My id is: {} - HASHCODE: {}", id, self.hashCode)
        val rnd = Round(getCRoundCount, coordinators, false)
        val configMsg = Configure(id, rnd)
        //val newState = state.copy(cval = Some(new CStruct()))
        log.debug("{} Configure using ROUND: {}", id, rnd)
        //context.become(proposerBehavior(config, phase1A(configMsg, newState, config)))
        
        val proposersSet = config.proposers.values.toSet
        proposersSet.foreach(_ ! RoundUpdate(rnd))

        context.become(proposerBehavior(config, phase1A(configMsg, state, config)))
      } else {
        log.debug("I'm NOT the LEADER! My id is {} - {}", id, self)
        log.info("I'm NOT the LEADER! My id is {} - {}", id, self)
      }

    case ClassicRound =>
      log.debug("TRYING TO RECONFIGURE USING A CLASSIC ROUND")
      if (coordinators contains self) {
        log.debug("I'm a LEADER! My id is: {} - HASHCODE: {}", id, self.hashCode)
        log.info("I'm a LEADER! My id is: {} - HASHCODE: {}", id, self.hashCode)
        val rnd = Round(getCRoundCount, coordinators, true)
        val configMsg = Configure(id, rnd)
        //val newState = state.copy(cval = Some(new CStruct()))
        log.debug("{} Configure using ROUND: {}", id, rnd)
        //context.become(proposerBehavior(config, phase1A(configMsg, newState, config)))

        //msgs2Bcollision = List[Msg2B]()

        val proposersSet = config.proposers.values.toSet
        proposersSet.foreach(_ ! RoundUpdate(rnd))

        context.become(proposerBehavior(config, phase1A(configMsg, state, config)))
      } else {
        log.debug("I'm NOT the LEADER! My id is {} - {}", id, self)
        log.info("I'm NOT the LEADER! My id is {} - {}", id, self)
      }

    case msg: CollisionRecovery =>
      log.debug("TRYING TO RECOVER FROM COLLISION AT ROUND {}", msg.rnd)
      if (coordinators contains self) {
        log.debug("I'm a LEADER! My id is: {} - HASHCODE: {} COLLISION RECOVERY", id, self.hashCode)
        log.info("I'm a LEADER! My id is: {} - HASHCODE: {} COLLISION RECOVERY", id, self.hashCode)

        if(msg.rnd == crnd) {
          //TODO: Create a recovery policy at settings
          //for now, recovering always as a Classic Round
          val rnd = Round(getCRoundCount, coordinators, true)
          val configMsg = Configure(id, rnd)

          log.debug("{} Configure using ROUND: {}", id, rnd)

          val proposersSet = config.proposers.values.toSet
          proposersSet.foreach(_ ! RoundUpdate(rnd))

          context.become(proposerBehavior(config, phase1A(configMsg, state, config)))
        } else {
          if(msg.rnd > crnd){ 
            log.debug("TEM ALGO ESTRANHO ACONTECENDO NA RECUPERACAO DE COLISAO")
            log.info("TEM ALGO ESTRANHO ACONTECENDO NA RECUPERACAO DE COLISAO")
          } else {
            log.debug("COLISAO JA ESTA SENDO TRATADA")
            log.info("COLISAO JA ESTA SENDO TRATADA")
          }
        }
      } else {
        log.debug("I'm NOT the LEADER! My id is {} - {}", id, self)
        log.info("I'm NOT the LEADER! My id is {} - {}", id, self)
      }

    case msg: RoundUpdate =>
      log.debug("PROPOSER ID {}, PRND {}, UPDATING TO ROUND {}",id, prnd, msg.rnd)
      checkAndUpdateRound(msg.rnd)

    case msg: UpdateConfig =>
      context.become(proposerBehavior(msg.config, state))

    case msg: UpdateRound =>
      if (grnd < msg.rnd) {
        grnd = grnd.copy(msg.rnd.count+1)
        crnd = crnd.copy(msg.rnd.count+1)
        val msgConfig = Configure(id, grnd)
        context.become(proposerBehavior(config, phase1A(msgConfig, state, config)))
      } else {
        log.debug("DID NOT UPDATE ROUND GRND: {} because it is greater than MSG.RND: {}", grnd, msg.rnd)
      }

    case msg: Msg1B =>
      log.debug("ID: {} -- RECEIVED {} -- FROM {}", id, msg, msg.senderId)
      //quorum1bMessages += (msg.senderId -> msg)
      quorum1bMessages.getOrElseUpdate(msg.rnd, MMap()) += (msg.senderId -> msg)
      context.become(proposerBehavior(config, phase2Start(msg, state, config)))

    case msg: Msg2A =>
      log.debug("ID: {} -- RECEIVED {} -- FROM {}", id, msg, msg.senderId)
      context.become(proposerBehavior(config, phase2AClassic(msg, state, config)))

    //case msg: Msg2B =>
      //log.debug("DETECTING COLLISION")
      //msgs2Bcollision :+= msg
      //log.debug("msgs2B at coordinator {}", msgs2Bcollision)
      //config.proposers.values.foreach(_ ! ClassicRound)
      
  }    

  def stashCommandState(config: ClusterConfiguration, state: ProposerMeta, comd: Cmd) (implicit ec: ExecutionContext): ProposerMeta = {
    if (state.pval == None){
      val cst = Some(Set(comd))
      log.info("COMMAND STASH {}", cst)
      val newState = state.copy(pval = cst)
      newState
    } else { 
      val commandSetSend = state.pval.map(a => a + comd)
      log.info("COMMAND STASH INCREMENTED {}", commandSetSend)
      val newState = state.copy(pval = commandSetSend)
      newState
    }
  }

  def propose(msg: Proposal, state: ProposerMeta, config: ClusterConfiguration)(implicit ec: ExecutionContext): ProposerMeta = {
    log.debug("trying to propose when cval != None")
    if (prnd == msg.rnd) {
      //checked previously if isCoordinatorOf(msg.rnd) && cval != None

      log.debug("PROPOSING state.cval before copy {}",state.cval)

      val proposedAsCStruct = new CStruct()
      //proposedAsCStruct.append(state.cval.get)
      proposedAsCStruct.append(msg.value.get)


      /*
      val proposedAsCStruct = state.cval.get.copy
      proposedAsCStruct.append(msg.value.get)
      */
      
      log.debug("PROPOSING state.cval after copy {}",state.cval)

      val newState = 
      if(state.pval != None) {
        //FIXME: after changing append to be polimorphic, I can do the following instead
        //requires further testing
        state.pval.map(setCmd => proposedAsCStruct.append(setCmd))
        
        //state.pval.map(c => c.map(d => proposedAsCStruct.append(d)))
        
        val emptyCmdSet: Option[Set[Cmd]] = Some(Set())

        //state.copy(pval = emptyCmdSet, cval = Some(proposedAsCStruct))
        state.copy(pval = emptyCmdSet)
      } else {
        //state.copy(cval = Some(proposedAsCStruct))
        state
      }

      msg.rnd.coordinator.foreach(_ ! Msg2A(id, msg.rnd, Some(proposedAsCStruct)))

      log.debug("PROPOSING newState.cval {}", newState.cval)

      newState
    } else {
      log.warning("PROPOSAL - ID - {} received proposal {}, but not able to propose. State: {}", id, msg, state)
      //FIXME: Find a better way to do this! 
      //retryBroadcast(self, Broadcast(msg.value.get(msg.senderId).value.asInstanceOf[Array[Byte]]))
      state
    }
  }

  def phase1A(msg: Configure, state: ProposerMeta, config: ClusterConfiguration): ProposerMeta = {
    log.debug("PHASE 1A: ID {} PRND {}, CRND {} ", id, prnd, crnd)
    if (isCoordinatorOf(msg.rnd) && crnd < msg.rnd) {
      val newState = state.copy(pval = state.pval, cval = None)
      checkAndUpdateRound(msg.rnd)
      log.debug("PHASE 1A UPDATING: ID {} PRND {}, CRND {} ", id, prnd, crnd)
      config.acceptors.values.foreach(_ ! Msg1A(id, msg.rnd))
      newState
    } else {
      log.error("PHASE1A - {} IS NOT COORDINATOR of ROUND: {}", id, msg.rnd)
      state
    }
  }

  def phase2Start(msg: Msg1B, state: ProposerMeta, config: ClusterConfiguration): ProposerMeta = {

    //I must remember to update quorumSize in config when initializing a Fast Round
    val q1Size = if (crnd.isClassic) settings.QuorumSize else settings.QuorumSizeFast

    log.debug("LEADER CVAL {}", state.cval)
    log.debug("LEADER q1Size {}, quorum1bMessages Size {}", q1Size, quorum1bMessages(msg.rnd).size)

    if (quorum1bMessages(msg.rnd).size == q1Size && isCoordinatorOf(msg.rnd) && crnd == msg.rnd && state.cval == None) {

      log.debug("PHASE2START - Proposer {} DID meet the quorum requirements with MSG ROUND: {} with state: {}", id, msg.rnd, state)

      //val msgs = quorum1bMessages.values.asInstanceOf[Iterable[Msg1B]]
      val msgs = quorum1bMessages(msg.rnd).values.asInstanceOf[Iterable[Msg1B]]
      val k = msgs.reduceLeft((a, b) => if(a.vrnd > b.vrnd) a else b).vrnd

      log.debug("PHASE2START PROPOSER ID {} - quorum1bMessages (agentID, RND, VRND, VVAL) {}", id, msgs)
      log.debug("PHASE2START PROPOSER ID {} - greatest known round from 1bMSGS {}", id, k)

      val q2Size = if(k.isClassic) settings.QuorumSize else settings.QuorumSizeFast

      /*
       //FOR TESTING
      val c4 = new Cmd(4)
      val c3 = new Cmd(3)
      val c2 = new Cmd(2)
      val c1 = new Cmd(1)

      val cseq1 = new CStruct()
      val cseq2 = new CStruct()
      val cseq3 = new CStruct()

      cset1.append(c1)

      cset2.append(c1)
      cset2.append(c2)

      cset3.append(c1)
      cset3.append(c2)
      cset3.append(c3)

      val vvalsRoundK = List(Some(cseq1),Some(cseq2),Some(cseq3))
      */

      /* 
       * Calculating the min and max length of combinations that will generate set G.
       * The following calculations work for different types of quorums, be it CC,FF or CF 
       * (where C and F stands for classic and Fast quorums respectively), where the first
       * type of quorum belongs to the coordinator and the second one to round K
       * We consider that a recovery can be done in a fast round.
       */

      val vvalsRoundK = msgs.filter(a => a.vrnd == k).map(v => v.vval).toList

      //Number of Acceptors must remain unchanged
      val gMinRange = (q1Size + q2Size) - settings.totalNrOfAcceptors
      val gMaxRange = vvalsRoundK.size

      log.debug("PHASE2START - gMinRange {}, gMaxRange {}", gMinRange, gMaxRange)

      //list of intersections that holds the Roundk.vvals that I will GLB to generate G
      var loi: List[List[CStruct]] = List()
      
      //Calculating the equivalent to Generalized Paxos's RS
      var combinationsCounter = 0
      for(combinationsCounter <- gMinRange to gMaxRange) {
        vvalsRoundK.combinations(combinationsCounter).toList.
        map(a => a.flatMap( (e: Option[CStruct]) => e)).
        map(f => if(f.nonEmpty) loi = loi :+ f)
      }

      log.debug("PHASE2START - LIST OF INTERSECTIONS {}", loi)
      
      /*  Calculating the proved safe W
       *  List of intersections is empty iff RS is empty
      */

      val prSafeW: Option[CStruct] = if(loi.isEmpty) {
        vvalsRoundK.head
        //vvalsRoundK.filter(v => v.vval.map(cs => !cs.isBottom)).toList.head
      } else {
        val g = loi.map(a => a.head.glb(a.tail.toSet))
        log.debug("Lista G {}", g)
        (g.head.lub(g.tail.toSet)).asInstanceOf[Option[CStruct]]
      }

      log.debug("PROOVED SAFE W: {}", prSafeW)
      
      val wExtended = 
        if(state.pval != None){
          //append the commands in the pval (commands stash) to provedsafe
          //extending it.
          val newProvedSafe = prSafeW.get
          state.pval.map(a => a.map(b => newProvedSafe.append(b)))
          Some(newProvedSafe)
        } else {
          prSafeW
        }
      
      log.debug("wExtended {}", prSafeW)
      //log.debug("wExtended {}", wExtended)

      val newState = 
        if(state.pval != None){
          val emptyCmdSet: Option[Set[Cmd]] = Some(Set())
          //When the stash of proposed commands is used, it must be emptied
          state.copy(pval = emptyCmdSet, cval = wExtended)
        } else {
          state.copy(cval = wExtended)
        }
      
      val acceptorsSet = config.acceptors.values.toSet
      acceptorsSet.foreach(_ ! Msg2A(id, msg.rnd, wExtended))

      //quorum1bMessages = scala.collection.mutable.Map[AgentId, Message]()

      newState
    } else {
      log.debug("PHASE2START - Proposer {} DID NOT meet the quorum requirements with MSG ROUND: {} with state: {}", id, msg.rnd, state)
      state
    }
  }

  def phase2AClassic(msg: Msg2A, state: ProposerMeta, config: ClusterConfiguration): ProposerMeta = {
    log.debug("REACHED PHASE2ACLASSIC")
    if (isCoordinatorOf(msg.rnd) && prnd == msg.rnd && state.cval != None && msg.rnd.isClassic) {

      log.debug("2ACLASSIC BEGINNING state.cval {}", state.cval)
      //val newVal = state.cval.get.copy
      
      val newVal = new CStruct()
      newVal.append(state.cval.get)

      log.debug("2ACLASSIC: OLD NEWVAL {}",newVal)

      //TODO: Gotta make Cstruct more flexible to avoid this kind of scary shit (OLD CODE)
      //msg.value.get.value.map(a => newVal.append(a))

      //TODO: Append is now polimorphic, gotta test this
      
      //if the option contained at msg.value is different from empty, it will be a cstruct and we append it to the new one.
      msg.value.map(c => newVal.append(c))

      log.debug("2ACLASSIC: NEW NEWVAL {}",newVal)

      val newState = state.copy(cval = Some(newVal))

      val acceptorsSet = config.acceptors.values.toSet
      acceptorsSet.foreach(_ ! Msg2A(id, msg.rnd, Some(newVal)))

      newState
    } else {
      log.debug("SOMETHING WRONG PHASE2ACLASSIC")
      state
    }
  }

}

class ProposerActor(val id: AgentId) extends Actor with Proposer {
  val settings = Settings(context.system)
  val waitFor = settings.MinNrOfNodes

  // Greatest known round
  var grnd: Round = Round()

  // Proposer current round
  var prnd: Round = Round()

  // Coordinator current round
  var crnd: Round = Round()

  var coordinators: Set[ActorRef] = Set()

  //Considering only 1 instance
  //old quorumPerInstance
  
  //var quorum1bMessages = scala.collection.mutable.Map[AgentId, Message]]()
  var quorum1bMessages = MMap[Round, MMap[AgentId, Message]]()

  //var msgs2Bcollision = List[Msg2B]()

  override def preStart(): Unit = {
    log.info("Proposer ID: {} UP on {}", id, self.path)
  }

  def isCoordinatorOf(round: Round): Boolean = (round.coordinator contains self)

  //val emptyCmdSet: Option[Set[Cmd]] = Some(Set())
  //Proposermeta(pval,cval)
  def receive = proposerBehavior(ClusterConfiguration(), ProposerMeta(None,None))(context.system.dispatcher)
}

object ProposerActor {
  def props(id: AgentId) : Props = Props(classOf[ProposerActor], id)
}
