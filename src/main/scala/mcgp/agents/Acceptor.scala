package mcgp.agents

import mcgp._
import mcgp.messages._
import mcgp.protocol._
import mcgp.cstructs._
import mcgp.commands._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext

import akka.actor._

trait Acceptor extends ActorLogging {
  this: AcceptorActor =>

  def phase1B(actorSender: ActorRef, msg: Msg1A, state: AcceptorMeta, config: ClusterConfiguration): AcceptorMeta = {

    log.debug("ACCEPTOR PHASE 1B - ID: {}, ACCEPTORS ROUND {}, MSG1B ROUND {}", id, state.rnd, msg.rnd)

    if (state.rnd < msg.rnd && (msg.rnd.coordinator contains actorSender)) {
      val newState = state.copy(rnd = msg.rnd)

      log.debug("ACCEPTOR ID: {} - PHASE1B UPDATING FROM ROUND {} TO ROUND {}", id, state.rnd, msg.rnd)
      actorSender ! Msg1B(id, newState.rnd, newState.vrnd, newState.vval)

      newState
    } else {
      log.warning("ID: {} - PHASE1B message round: {} IS LESS THAN OR EQUAL state rnd: {}", id, msg.rnd, state.rnd)
      actorSender ! UpdateRound(state.rnd)

      state
    }
  }

  def phase2BClassic(msg: Msg2A, state: AcceptorMeta, config: ClusterConfiguration): AcceptorMeta = {
    //TODO:test this
    //if (state.rnd < msg.rnd || (state.rnd == msg.rnd && (state.vval.get.isPrefixOf(msg.value.get) && state.vval.get != msg.value.get))) {
    
    log.debug("REACHED 2BCLASSIC AT ACCEPTORS")
    log.debug("ID: {} - Message round: {} -- state rnd: {}", id, msg.rnd, state.rnd)

    //if (state.rnd < msg.rnd || (state.rnd == msg.rnd && (state.vval.get.isPrefixOf(msg.value.get)))) {
    
    if (state.vrnd < msg.rnd || (state.vrnd == msg.rnd && (state.vval.get.isPrefixOf(msg.value.get)))) {
      val newState = state.copy(vrnd = msg.rnd, vval = msg.value)

      log.debug("ACCEPTOR ID: {} - PHASE2BCLASSIC UPDATING VVAL TO {}", id, newState.vval)
      config.learners.values foreach (_ ! Msg2B(id, msg.rnd, newState.vval))

      newState
    } else {
      log.debug("SOMETHING WRONG PHASE2BCLASSIC")
      state
    }
  }

  //o que dispara 2Bfast eh um commandProposal durante um round fast
  def phase2BFast(msg: Proposal, state: AcceptorMeta, config: ClusterConfiguration): AcceptorMeta = {

    //na tese diz que ja deve ter aceitado algo para o round da msg, isso procede?
    log.debug("ID: {} - Message round: {} -- state rnd: {}", id, msg.rnd, state.rnd)
    if(state.vrnd == msg.rnd && state.vval != None) {

      val newVal = new CStruct()
      newVal.append(state.vval.get)
      newVal.append(msg.value.get)

      /* simulating a collision
      
      val c2 = new Cmd(2)
      val c3 = new Cmd(3)
      val c4 = new Cmd(4)
      if(id == "a1")
        newVal.append(c2)
      if(id == "a2")
        newVal.append(c3)
      if(id == "a3")
        newVal.append(c4)
      */

      val newState = state.copy(vval = Some(newVal))
      config.learners.values foreach (_ ! Msg2B(id, msg.rnd, newState.vval))

      newState
    } else {
      log.debug("SOMETHING WRONG PHASE2BFAST")
      state
    }
  }

  def acceptorBehavior(config: ClusterConfiguration, state: AcceptorMeta)(implicit ec: ExecutionContext): Receive = {
    case GetState =>
      log.info("ID: {} -- HAS STATE: {}", id, state)

    case msg: Msg1A =>
      log.debug("ID: {} -- RECEIVED {} FROM {}", id, msg, msg.senderId)
      context.become(acceptorBehavior(config, phase1B(sender, msg, state, config)))

    case msg: Msg2A =>
      log.debug("ID: {} -- RECEIVED {} FROM {}", id, msg, msg.senderId)
      log.debug("MSG.RND = {}", msg.rnd)
      context.become(acceptorBehavior(config, phase2BClassic(msg, state, config)))
    
    case msg: Proposal =>
      if(!msg.rnd.isClassic) {
        context.become(acceptorBehavior(config, phase2BFast(msg, state, config)))
      } else {
        context.become(acceptorBehavior(config, state))
        log.debug("Acceptor {} received a command proposal, but THIS IS NOT A FAST ROUND", id)
      }

    case msg: UpdateConfig =>
      context.become(acceptorBehavior(msg.config, state))
  }
}

class AcceptorActor(val id: AgentId) extends Actor with Acceptor {
  override def preStart(): Unit = {
    log.info("Acceptor ID: {} UP on {}", id, self.path)
  }

  //Every acceptor starts with vval = Bottom, where bottom is the empty CStruct
  val bottomValue = new CStruct()
  def receive = acceptorBehavior(ClusterConfiguration(), AcceptorMeta(Round(), Round(), Some(bottomValue)))(context.system.dispatcher)
}

object AcceptorActor {
  def props(id: AgentId) : Props = Props(classOf[AcceptorActor], id)
}
