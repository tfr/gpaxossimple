package mcgp.protocol

import mcgp._

case class ProposerMeta(
  //pval: Option[CStruct], //talvez o correto seja mesmo uma CStruct
  pval: Option[Set[Cmd]], //se for um singletonCstruct, nao vou conseguir armazenar varios comandos, melhor ser uma collection mesmo.
  cval: Option[CStruct] // MaxLdrTried
)extends Serializable

case class AcceptorMeta(
  rnd: Round, //mbal[a]
  vrnd: Round, // bal[a]
  vval: Option[CStruct] // val[a]
)extends Serializable

case class LearnerMeta(
  learned: Option[CStruct] //learned[l]
)extends Serializable
