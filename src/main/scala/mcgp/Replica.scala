package mcgp

import akka.cluster.{ Cluster, MemberStatus }
import akka.actor.{ Actor, ActorRef }
import akka.actor.{ Address, ActorPath, RootActorPath }
import akka.actor.Props
import akka.actor.ActorLogging
import akka.cluster.ClusterEvent._
import akka.actor.ExtendedActorSystem
import akka.cluster.client.ClusterClientReceptionist
import akka.actor.SupervisorStrategy.{ Restart, Stop }
import akka.actor.OneForOneStrategy

//import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random
import scala.collection.immutable.{ Set, Map }

import mcgp.messages._
import mcgp.agents._
import mcgp.cstructs._
import mcgp.commands._
import mcgp.serialization.MCGPserializer

/*
 * Cluster replica
 * This node will handle the request of some client command
 */

class Replica extends Actor with ActorLogging {
  //TODO: cleanup and change name to replica
  val serializer = new MCGPserializer(context.system.asInstanceOf[ExtendedActorSystem])
  val cluster = Cluster(context.system)
  val settings = Settings(context.system)
  val nodeId = settings.NodeId
  //FIXME:this is bad code, this should be passed in the config
  //parameter is hardcoded
  val nodeNum = nodeId.splitAt(4)._2.toInt
  val rCmds = settings.RandomNrOfCmds
  val waitFor = settings.MinNrOfNodes
  val quorumSize = settings.QuorumSize
  val quorumSizeFast = settings.QuorumSizeFast
  val nodeAgents = settings.NrOfAgentsOfRoleOnNode
  val proposersIds = settings.ProposerIdsByName
  val learnersIds = settings.LearnerIdsByName
  val acceptorsIds = settings.AcceptorIdsByName
  val protocolRoles = settings.ProtocolRoles

  log.info("NODE AGENTS: {}", nodeAgents)
  
  // Agents of the protocol
  var proposers = Map.empty[AgentId, ActorRef]
  var acceptors = Map.empty[AgentId, ActorRef]
  var learners  = Map.empty[AgentId, ActorRef]

  // Associated clients
  var clients = Set.empty[ActorRef]

  // Associated servers
  var servers = Set.empty[ActorRef]

  for((name, id) <- proposersIds) {
    proposers += (id -> context.actorOf(ProposerActor.props(id), name=s"$name")) 
  }

  for ((name, id) <- learnersIds) {
    learners += (id -> context.actorOf(LearnerActor.props(id), name=s"$name"))
  }

  for ((name, id) <- acceptorsIds) {
    acceptors += (id -> context.actorOf(AcceptorActor.props(id), name=s"$name"))
  }
  
  log.info("PROPOSERS: {}", proposers)
  log.info("ACCEPTORS: {}", acceptors)
  log.info("LEARNERS:  {}", learners)

  // A Set of nodes(members) in the cluster that this node knows about
  var nodes = Set.empty[Address]
  
  var members = Map.empty[ActorRef, ClusterConfiguration]

  val console = context.actorOf(Props[ConsoleClient], "console")

  //TODO: Set the Oracle class based on configuration
  val leaderOracle = context.actorOf(Props[LeaderOracle], "leaderOracle")

  //val cfproposerOracle = context.actorOf(Props[CFProposerOracle], "cfproposerOracle")

  val myConfig = ClusterConfiguration(proposers, acceptors, learners)

  log.info("Registering Recepcionist on node: {}", nodeId)
  //ClusterReceptionistExtension(context.system).registerService(self)
  ClusterClientReceptionist(context.system).registerService(self)

  // Subscribe to cluster changes, MemberUp
  override def preStart(): Unit = {
    cluster.subscribe(self, classOf[UnreachableMember])
  }

  // Unsubscribe when stop to re-subscripe when restart
  override def postStop(): Unit = {
    cluster.unsubscribe(self)
  }

  def memberPath(address: Address): ActorPath = RootActorPath(address) / "user" / "node"
  
  def notifyAll(config: ClusterConfiguration) = {
    for (p <- proposers.values if !proposers.isEmpty) { p ! UpdateConfig(config) }
    for (a <- acceptors.values if !acceptors.isEmpty) { a ! UpdateConfig(config) }
    for (l <- learners.values  if !learners.isEmpty)  { l ! UpdateConfig(config) }
  }

  def getRandomAgent(from: Vector[ActorRef]): Option[ActorRef] = 
    if (from.isEmpty) None
    else Some(from(Random.nextInt(from.size)))

  // scala Set to Java Set Converters
  def scalaToJavaSet[T](scalaSet: Set[T]): java.util.Set[T] = {
    val javaSet = new java.util.HashSet[T]()
    scalaSet.foreach(entry => javaSet.add(entry))
    javaSet
  }

  // FIXME: Only do this step if all configured nodes are UP. How know that? Singleton?
  def getBroadcastGroup(): java.util.Set[ActorRef] = scalaToJavaSet[ActorRef](members.keys.toSet)

  //TODO Use ClusterSingleton to manager ClusterClient requests and a Router to select agents
  def registerClient(client: ActorRef): Unit = {
    clients += client
    val group = getBroadcastGroup
    val proposer = getRandomAgent(proposers.values.toVector)
    log.info("Registering client: {} with proposer: {}", client, proposer)
    if (proposer != None)
      client ! ClientRegistered(proposer.get, group)
    else
      log.error("{} does not have a proposer: {}!", self, proposer)
  }

  def registerServer(server: ActorRef): Unit = {
    servers += server
    val learner = getRandomAgent(learners.values.toVector)
    log.info("Registering server: {} with learner: {}", server, learner)
//    learner ! ReplyLearnedValuesTo(server)
  }

  override val supervisorStrategy = OneForOneStrategy(loggingEnabled = false) {
    case e =>
      log.error("EXCEPTION: {} ---- MESSAGE: {} ---- PrintStackTrace: {}", e, e.getMessage, e.printStackTrace)
      Stop
  }

  def receive = configuration(myConfig)

  def configuration(config: ClusterConfiguration): Receive = {
    case StartConsole => console ! StartConsole

    //case Done => println("RECEBIDO")
    // FIXME: Remove this awful test
    case ConsoleInput(cmd: String) =>
      log.debug("Received COMMAND {} ", cmd)
      cmd match {
        case "pstate"   => proposers.values.foreach(_ ! GetState) 
        case "astate"   => acceptors.values.foreach(_ ! GetState) 
        case "lstate"   => learners.values.foreach(_ ! GetState)
        case "print"    => acceptors.values.foreach(_ ! "print")
        case "config"   => log.info("{} - CONFIG: {}", nodeId, config)
        case "fast"     => proposers.values.foreach(_ ! FastRound)
        case "classic"  => proposers.values.foreach(_ ! ClassicRound)
        case "t"  => 
            log.info("PROPONDO VARIOS COMANDOS ALEATORIOS, na replica {}",nodeId)
            if(proposers.nonEmpty) {
              val min_nr_cmds = nodeNum * rCmds
              val max_nr_cmds = min_nr_cmds + rCmds
              val set_nums = Set(min_nr_cmds to max_nr_cmds) 

              val set_int = set_nums.head.toSet
              val set_cmds = set_int.map(el => new Cmd(el))

              log.info("Set cmds {}", set_cmds)
              set_cmds.map(cm => proposers.values.toVector(Random.nextInt(proposers.size)) ! CommandProposal(cm))

              //proposers.values.toVector(Random.nextInt(proposers.size)) ! CommandProposal(c1)
            } else {
              log.info("No proposers on this node")
            }
        /*
        case "all"      => 
          config.proposers.values.zipWithIndex.foreach { case (ref, i) =>
            ref ! Broadcast(serializer.toBinary(cmd ++ "_" ++ i.toString))
          }
        */
        case _ => 
          log.info("PROPONDO COMANDO INTEIRO")
          if(proposers.nonEmpty) {
            val n_cmd = cmd.toInt
            val c1 = new Cmd(n_cmd)
            //val data = serializer.toBinary(cmd)
            proposers.values.toVector(Random.nextInt(proposers.size)) ! CommandProposal(c1)
          } else {
            log.info("Oops!! This node [{}] doesn't have a proposer to handle commands.", nodeId)
          }
          
      }

    //TODO identify when a client or a server disconnect and remove them.
    case msg: RegisterClient => registerClient(msg.client)

    case msg: RegisterServer => registerServer(msg.server)

    //TODO: Remove this
    case state: CurrentClusterState =>
      log.info("Current members: {}", state.members)
      nodes = state.members.collect {
        case m if m.status == MemberStatus.Up => m.address
      }

    case msg: UpdateConfig =>
      log.debug("Node: {} update config to: {}", nodeId, msg.config)
      notifyAll(msg.config)
      context.become(configuration(msg.config))
      sender ! Done

    // Get the configuration of some member node
    case GiveMeAgents =>
      sender ! GetAgents(self, myConfig)

    case UnreachableMember(member) =>
      log.warning("Member {} detected as unreachable: {}", self, member)
      //TODO notify the leaderOracle and adopt new police

    case m =>
      log.error("A unknown message [ {} ] received!", m)
  }
}
