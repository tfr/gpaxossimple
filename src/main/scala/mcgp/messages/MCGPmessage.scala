package mcgp.messages

import scala.beans.BeanProperty
import akka.actor.ActorRef
import java.util.Set

/*
 *  * Client Messages
 *   */

sealed class MCGPmessage extends Serializable 

//case class Broadcast(@BeanProperty val data: Array[Byte]) extends MCGPmessage
case class Delivery(@BeanProperty val data: Array[Byte])  extends MCGPmessage

case class RegisterClient(client: ActorRef) extends MCGPmessage
case class RegisterServer(server: ActorRef) extends MCGPmessage

case class ClientRegistered(
    @BeanProperty val proposer: ActorRef, 
      @BeanProperty val group: Set[ActorRef])  extends MCGPmessage
