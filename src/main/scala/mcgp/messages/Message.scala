package mcgp.messages

import akka.actor.ActorRef

import mcgp._
import mcgp.commands._

/**
 *  * Define protocol messages for Generalized Paxos and Multicoordinated Generalized Paxos.
 *   */
sealed class Message extends Serializable

//Message sent by proposer p to the  coordinator of the current round of p.
case class Proposal(senderId: AgentId, rnd: Round, value: Option[Cmd]) extends Message

//TODO: change comd to Set of cmd
case class CommandProposal(comd: Cmd) extends Message

// Message sent by coordinator c to all acceptors.
case class Msg1A(senderId: AgentId, rnd: Round) extends Message

// only used in the case of multiple instances
//case class Msg1Am(senderId: AgentId, rnd: Round) extends Message

// Message sent by acceptor a to the coordinator of round rnd.
case class Msg1B(senderId: AgentId, rnd: Round, vrnd: Round, vval: Option[CStruct]) extends Message

// Message sent by coordinator to all proposers and/or acceptors.
//case class Msg2S(senderId: AgentId, instance: Instance, rnd: Round, value: Option[VMap[AgentId, Values]]) extends Message

// Message sent by proposer to all acceptors on the same round rnd.
case class Msg2A(senderId: AgentId, rnd: Round, value: Option[CStruct]) extends Message

// Message sent by acceptor a to all learners.
case class Msg2B(senderId: AgentId, rnd: Round, value: Option[CStruct]) extends Message

// Message sent by learners to all Agents if something was learned.
case object Learn extends Message

// Message sent to start the protocol (Phase1)
//case class Configure(senderId: AgentId, instance: Instance, rnd: Round) extends Message

//Considering 1 instance, for now
case class Configure(senderId: AgentId, rnd: Round) extends Message

case class CollisionRecovery(rnd: Round) extends Message
/*
 * Auxiliary messages
 */

// Message sent to propose a new value
//case class TryPropose(value: Values) extends Message

//case object GetCFPs extends Message

//case class Learned(instance: Instance, vmap: Option[VMap[AgentId, Values]]) extends Message
//case class DeliveredVMap(vmap: Option[VMap[AgentId, Values]]) extends Message
//case class Deliver(instance: Instance, proposerId: AgentId, learned: Option[VMap[AgentId, Values]]) extends Message

case object GetState extends Message
case object FastRound extends Message
case object ClassicRound extends Message

//case object GetIntervals extends Message
//case class TakeIntervals(interval: IRange) extends Message

case class UpdateRound(rnd: Round) extends Message
case object Done extends Message

case class RoundUpdate(rnd: Round) extends Message

//case class UpdateRound(instance: Instance, rnd: Round) extends Message

/*
 * Cluster Messages
 */
case class UpdateConfig(config: ClusterConfiguration) extends Message
case class GetAgents(ref: ActorRef, config: ClusterConfiguration) extends Message
case object GiveMeAgents extends Message

/*
 * Leader Election Messages
 */

case class MemberChange(config: ClusterConfiguration, notifyTo: Set[ActorRef]) extends Message
case class NewLeader(coordinators: Set[ActorRef]) extends Message
case object WhoIsLeader

/*
 * Collision-fast Oracle Messages
 */
case class ProposerSet(replyTo: ActorRef, proposersRef: Set[ActorRef]) extends Message

/*
 * Console Messages
 */
sealed class ConsoleMsg
case object StartConsole extends ConsoleMsg
case class ConsoleInput(cmd: String) extends ConsoleMsg
