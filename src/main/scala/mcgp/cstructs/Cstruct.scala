package mcgp.cstructs

import mcgp.commands._

case class IncompatibleCStructsException() extends Exception{}

abstract class Cstruct {
  type T
  var value: T
  def append[C <: Command](value: Set[C]): Unit
  def append(value: Command): Unit
  //I mainly use this for copying cstructs
  def append(value: Cstruct): Unit
  def contains(value: Command): Boolean
  def isBottom: Boolean
  def glb(cs: Set[Cstruct]): Cstruct
  def lub(cs: Set[Cstruct]): Option[Cstruct]
  def isPrefixOf(that: Cstruct): Boolean

}
