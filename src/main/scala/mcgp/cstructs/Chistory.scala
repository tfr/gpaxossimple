package mcgp.cstructs

import mcgp.commands._
import scalax.collection.Graph // or scalax.collection.mutable.Graph
import scalax.collection.GraphPredef._, scalax.collection.GraphEdge._
import scala.collection.mutable.LinkedHashSet

import scala.util.control.Breaks._

case class Chistory() extends Cstruct {
  type T = Graph[Command, DiEdge]
  var value: T = Graph[Command, DiEdge]()
  var sequence: LinkedHashSet[Command] = LinkedHashSet()

  def isBottom = value.isEmpty

  //This is horrendous, when everything is working properly
  //I will change "var value" to "val value" and pass it
  //as a constructor parameter, doing everything in a more functional way.
  
  override def append(cmd: Command) = { 
    this.sequence += cmd
    if(!this.contains(cmd)) {
       this.value += cmd
       val copyVal= this.value
       val trash = copyVal.nodes.map(n => if(!n.value.commutesWith(cmd)) this.value += (n.value~>cmd))
    }
  }

  override def append(cst: Cstruct) = { 
    cst.asInstanceOf[Chistory].sequence.map(c => this.append(c))
  }

  override def append[C <: Command](setCmd: Set[C]) = setCmd.map(e => this.append(e))

  // equals is Not safe enough

  def canEqual(a: Any) = a.isInstanceOf[Chistory]
  override def equals(that: Any): Boolean =
    that match {
      case that: Chistory => (that.canEqual(this) && (this.value == that.value))
      case _ => println("Equals Chistory Tipo errado!"); false
    }
  override def hashCode: Int = {
    val prime = 31
    var result = 1
    result = prime * result + value.hashCode;
    return result
  }
 
  def checkPrefix(ch: Chistory, cs: Set[Chistory]): Boolean = {
    val ans = cs.map(h => if(ch.isPrefixOf(h)) true else false)
    if (ans.contains(false)) false else true
  }

  override def glb (cs: Set[Cstruct]): Cstruct = {
    val setHistory = Set(this) ++ cs.asInstanceOf[Set[Chistory]]
    val largestHistory = setHistory.reduce((x,y) => if (x.value.nodes.size > y.value.nodes.size) x else y)
    
    var ans = Chistory()

    //FIXME: make it breakable
    var foundAns = false
    val trash = largestHistory.sequence.map(c => 
          { 
            //messed up copy.
            //should make cstruct functional and pass as param: cstruct.append(c)
            //returning a new cstruct with c.
            val param = Chistory();
            param.append(ans);
            param.append(c);

            if(checkPrefix(param, setHistory) && (!foundAns)) {
              ans.append(c) 
            } else {
              //stop adding commands to ans after finding the right prefix
              foundAns = true
              ans
            }
          }
        )
    ans 
  }

  def areCompatible(ch1: Chistory, ch2: Chistory): Boolean = {
    val (g1, g2) = (ch1.value, ch2.value)

    val (g1Outer, g2Outer) = (g1.nodes.toOuter,g2.nodes.toOuter)

    val commuteC = g1Outer diff g2Outer
    val commuteD = g2Outer diff g1Outer

    var fstAns = true
    val commuteCandD = breakable { 
      commuteC.map(c => 
          commuteD.map(d =>
           {
             if(!c.commutesWith(d)){
               fstAns = false 
               break
             } else {
               fstAns = true
             }
           }
          )
      )
    }
    
    if(!fstAns) {
      false
    } else {
      //iff the subgraphs of ch1 and ch2 consisting of the nodes their have in common are identical
      val nodesInCommon = g1Outer intersect g2Outer
      val subG1 = g1 filter ((c:Command) => nodesInCommon.contains(c))
      val subG2 = g2 filter ((c:Command) => nodesInCommon.contains(c))

      if (subG1 == subG2) {
        true
      } else {
        false
      }
    }
    
  }
  override def lub(cs: Set[Cstruct]): Option[Cstruct] = {
    val setHistory = Set(this) ++ cs.asInstanceOf[Set[Chistory]]
    var ans = Chistory()
    var ans2 = true
    
    breakable{
      setHistory.asInstanceOf[Set[Chistory]].reduce((a,b) => 
      {
        // acho que usar sequence vai dar merda
        // o correto seria usar tudo como grafo e colocar
        // o primeiro comando como inicial pra poder percorrer
        // pq ai fazer o glb nao precisaria fazer como fiz
        // a unica coisa que mudaria aqui seria que faria apenas  uniao de 2 grafos
        if(areCompatible(a,b)) {
           val newSeq = a.sequence union b.sequence
           newSeq.map(c => ans.append(c))
           ans
         } else {
           ans2 = false
           break
         } 
      } ) 
    }
    
    if(!ans2){
      None
    } else {
      //HACK
      if(setHistory.size == 1)
        ans = setHistory.head
      Some(ans)
    }
   //None
  }

  //if this is prefix of that, returns true
  override def isPrefixOf(that: Cstruct): Boolean = {
    val thisGraph = this.value
    val thisNodes = thisGraph.nodes
    val thatGraph = that.asInstanceOf[Chistory].value
    var ans = true
    breakable
    {
       thisNodes.map(n => 
       {
         val intersectNode = thatGraph find n;
         if(intersectNode == None) {
           //its not a subgraph
           ans = false
           break
         } else {
           //return the set of nodes that are direct predecessor and take the values inside the nodes
           //to compare if both sets are equal
           //TODO: Change to diPredecessors.toOuterNode
           val thisIncoming = n.diPredecessors.map(e => e.value)
           val thatIncoming = intersectNode.get.diPredecessors.map(e => e.value)
           if(thisIncoming == thatIncoming) {
             ans = true
           } else { 
             ans = false
             break
           }
         }
       })
    }
    ans
  }

  override def contains(find: Command): Boolean = {
    this.value.contains(find)
  }
 
  override def toString = s"CHIST: " + value.toString
}
