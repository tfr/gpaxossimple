package mcgp.cstructs

import mcgp.commands._

case class Cset() extends Cstruct {
  type T = Set[Command]
  var value: T = Set[Command]()

  def isBottom = value.isEmpty

  override def append(cmd: Command) = value += cmd
  override def append(cst: Cstruct) = cst.asInstanceOf[Cset].value.map(e => value += e)
  override def append[C <: Command](setCmd: Set[C]) = setCmd.map(e => value += e)

  // The following way to compare Csets does not look safe enough
  def canEqual(a: Any) = a.isInstanceOf[Cset]
  override def equals(that: Any): Boolean =
    that match {
      case that: Cset => (that.canEqual(this) && (this.value == that.value))
      case _ => println("Equals Cset Tipo errado!"); false
    }
  override def hashCode: Int = {
		val prime = 31
		var result = 1
		result = prime * result + value.hashCode;
		return result
  }

  //glb in a set is pure intersection
  override def glb (cs: Set[Cstruct]): Cstruct = {
      val cs2 = Set(this) ++ cs.asInstanceOf[Set[Cset]]
      cs2.reduce((a,b) => a prefix b)
  }
  def prefix(that: Cset): Cset = {
    val ret = new Cset()
    ret.value = this.value.intersect(that.value)
    ret
  }

  override def isPrefixOf(that: Cstruct): Boolean ={
   this.asInstanceOf[Cset].value.subsetOf(that.asInstanceOf[Cset].value)
  }

  //lub in a set is pure union
  //Csets will never be incompatible
  
  override def lub(cs: Set[Cstruct]): Option[Cstruct] = {
    val ret = new Cset() 
    val cs2 = Set(this) ++ cs.asInstanceOf[Set[Cset]]
    ret.value = (cs2.map(a => a.value)).flatten
    Some(ret)
  }

  override def contains(find: Command): Boolean = {
    this.value.contains(find)
  }
  override def toString = s"CSET: " + value.toString
}
