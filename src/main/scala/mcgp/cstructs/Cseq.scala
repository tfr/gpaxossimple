package mcgp.cstructs

import mcgp.commands._

case class Cseq() extends Cstruct {
  type T = List[Command]
  var value: T = List[Command]()

  def isBottom = value.isEmpty

  override def append(cmd: Command) = value :+= cmd
  
  /*
  override def append(cmd: Command): Cstruct = {
    var ret = Cseq()
    ret.value = this.value :+ cmd
    ret
  }
  */

  override def append(cst: Cstruct) = cst.asInstanceOf[Cseq].value.map(e => value :+= e)
  override def append[C <: Command](setCmd: Set[C]) = setCmd.map(e => value :+= e)

  // equals is Not safe enough

  def canEqual(a: Any) = a.isInstanceOf[Cseq]
  override def equals(that: Any): Boolean =
    that match {
      case that: Cseq => (that.canEqual(this) && (this.value == that.value))
      case _ => println("Equals Cseq Tipo errado!"); false
    }
  override def hashCode: Int = {
    val prime = 31
    var result = 1
    result = prime * result + value.hashCode;
    return result
  }
 
  override def glb (cs: Set[Cstruct]): Cstruct = {
    val cs2 = Set(this) ++ cs.asInstanceOf[Set[Cseq]]
    val ret = Cseq()
 
    val listOfValues = cs2.asInstanceOf[Set[Cseq]].map(e => e.value)
    val glbResult = listOfValues.reduce((a,b) => (a,b).zipped.takeWhile(v => v._1 == v._2).unzip._1.toList)
    glbResult.map(g => ret.append(g))
 
    ret
  }
 
  //LUB({1,2,3},{1,2,4}) não existe
  //LUB({1},{1,2},{1,2,4}) = {1,2,4}
  //por que {1,2,4} é a única sequencia da qual todas as outras são prefixos.
  //Considering Cseqs, the lub is not union, List(a,b,c) is not prefix of List(a,b,d,c)
  
  override def lub(cs: Set[Cstruct]): Option[Cstruct] = {
    val cs2 = Set(this) ++ cs.asInstanceOf[Set[Cseq]] 
    try {
      Some(cs2.asInstanceOf[Set[Cseq]].reduce((a,b) =>
      if(a.value.startsWith(b.value)) a 
      else if(b.value.startsWith(a.value)) b
      else throw new IncompatibleCStructsException()))
    } catch {
      case _: IncompatibleCStructsException => None
    }
  }
 
  //V prefix W -> 
  //W = V ** SeqCmds
  //if (this) is prefix of (that)
  
  override def isPrefixOf(that: Cstruct): Boolean = {
    that.asInstanceOf[Cseq].value.startsWith(this.value)
  }
 
  override def contains(find: Command): Boolean = {
    this.value.contains(find)
  }
 
  override def toString = s"CSEQ: " + value.toString
}
