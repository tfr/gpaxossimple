package mcgp

import akka.actor.ActorRef

case class Round(count: Int, coordinator: Set[ActorRef], roundClassic: Boolean) extends Ordered[Round] {

  //Result of comparing this with operand that. returns x where x < 0 iff this < that x == 0 iff this == that x > 0 iff this > that
  override def compare(that: Round) = (this.count - that.count) match {
    //FIXME: Not get only the head of coordinators
    case 0 if (!coordinator.isEmpty) => (this.coordinator.head.hashCode - that.coordinator.head.hashCode)
    case n => n.toInt
  }

  def inc(): Round = Round(this.count + 1, this.coordinator, true)

  //True stands for Classic Round, otherwise its a Fast Round
  def isClassic(): Boolean = this.roundClassic

  override def toString = s"< " + count + "; " + { if(coordinator.nonEmpty) coordinator.head.hashCode else coordinator } + "; " + {if(this.isClassic) "Classic" else "Fast"} + " >"
}

object Round {
  def apply() = new Round(0, Set(), true)
}
