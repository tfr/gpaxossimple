package mcgp.commands

abstract class Command {
    type A
    val content: A
    def commutesWith(that: Command): Boolean
}
