package mcgp.commands

case class CommutativeCommandInt(value: Int) extends Command {
  type A = Int
  val content: A = value

  //for testing
  val cM = Map( 1 -> List(1,4,5), 
                2 -> List(2,3,5),
                3 -> List(3,2,4),
                4 -> List(4,1,3,5),
                5 -> List(5,1,4,2)
              )
  //A simple commutation rule, so that we can
  //generate sets with random cmds with some proportion of them commuting
  override def commutesWith(that: Command): Boolean = {
    val fst: Int = this.content
    val snd: Int = that.asInstanceOf[CommutativeCommandInt].content

    //for testing cM
    /*
    val trying = cM get fst
    if(trying != None && trying.get.contains(snd)) {
      true
    } else { 
      if(fst == snd)
        true
      else
        false
    }
    */

    //They commute if both are even or both are odd, or both are equal
    if (fst == snd) {
      true
    } else {
      if ( ((fst %2 == 0) && (snd %2 == 0)) || ((fst %2 != 0) && (snd %2 != 0)) )
        true
      else
        false
    }
      /*
      if( (fst >= 1 && snd >= 1) && (fst <= 5 && snd <= 5) ) 
        true 
      else 
        false
      */
  }

  /* The following way to compare commands may be faulty */

  def canEqual(a: Any) = a.isInstanceOf[CommutativeCommandInt]
  override def equals(t: Any): Boolean =
    t match {
      case that: CommutativeCommandInt => that.canEqual(this) && this.hashCode == that.hashCode
      case _ => println("Equals CommutativeCommandInt Tipo errado!"); false
    }

  override def hashCode: Int = {
  	val prime = 31
  	var result = 1
  	result = prime * result + content
  	return result
  }

  override def toString = "CCI " + content.toString
}
