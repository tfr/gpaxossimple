package mcgp.commands

case class SimpleCommandInt(value: Int) extends Command {
  type A = Int
  val content: A = value

  //this should return false
  override def commutesWith(that: Command): Boolean = true
  /*
    c match {
      case sci: SimpleCommandInt => (this.content + sci.content) % 2 == 0
      case _ => println("CommuteWith Tipo errado!"); false
    }
  */

  /* The following way to compare commands may be faulty */

  def canEqual(a: Any) = a.isInstanceOf[SimpleCommandInt]

  override def equals(t: Any): Boolean =
    t match {
      case that: SimpleCommandInt => that.canEqual(this) && this.hashCode == that.hashCode
      case _ => println("Equals SimpleCommandInt Tipo errado!"); false
    }

  override def hashCode: Int = {
  	val prime = 31
  	var result = 1
  	result = prime * result + content
  	return result
  }
    //type SimpleCommandInt = Command { type A = Int } 
    //def simpleCommandInt(x: Int): SimpleCommandInt = new Command { ... }

  override def toString = "SCI " + content.toString
}
