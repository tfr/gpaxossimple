/*
  val c1: Command = new SimpleCommandInt(1)
  val c2: Command = new SimpleCommandInt(2)
  val c3: Command = new SimpleCommandInt(3)
  val c4: Command = new SimpleCommandInt(4)
  val c7: Command = new SimpleCommandInt(1)

  val cset1: Cstruct = new Cset()
  cset1.append(c1)
  cset1.append(c2)
  cset1.append(c3)
  cset1.append(c4)

  val cset2: Cstruct = new Cset()
  cset2.append(c1)
  cset2.append(c2)
  cset2.append(c3)
  cset2.append(c7)
  cset2.append(c4)


  println("First Cset\n"+cset1+"\n")

  println("Second Cset\n"+cset2+"\n")

  println("Both Csets that contain SimpleCommandInts, are equal if the following is true:")
  println(cset1 == cset2)
*/

/*
  val c1: Command = new SimpleCommandInt(1)
  val c2: Command = new SimpleCommandInt(2)
  val c3: Command = new SimpleCommandInt(3)
  val c4: Command = new SimpleCommandInt(4)
  val c7: Command = new SimpleCommandInt(1)
  
  val cset1: Cstruct = new Cset()
  cset1.append(c1)
  cset1.append(c2)
  cset1.append(c3)
  cset1.append(c4)

  val cset2: Cstruct = new Cset()
  cset2.append(c1)
  cset2.append(c2)
  cset2.append(c3)
  cset2.append(c7)
  cset2.append(c4)


  println("First Cset\n"+cset1+"\n")

  println("Second Cset\n"+cset2+"\n")

  println("Both Csets that contain SimpleCommandInts, are equal if the following is true:")
  println(cset1 == cset2)

	val setiii = LinkedHashSet(cset1,cset2)
	println(setiii)
*/



//  val cset3: Cstruct = new Cset()
//  cset3.append(c1)
//
//  println("Third Cset\n"+cset3+"\n")
//
//  val cq: Cstruct = new Cseq()
//  val cs = Set(cset1,cset2)
//  println("Set containing Cset 1, 2")
//  println(cs)
//
//  val cset_lub_test = cset3.lub(cs)
//  println("Lub between Cset1,Cset2 and Cset3")
//  println(cset_lub_test)
//
//  val cset_glb_test = cset3.glb(cs)
//  println("Glb between Cset1,Cset2 and Cset3")
//  println(cset_glb_test)
//
//
//  var csing: Cstruct = new Csingleton()
//  println(csing)
//  csing.append(c1)
//  println(csing)
//  csing.append(c2)
//  println(csing)
//
//
//  val cseq1: Cstruct = new Cseq()
//  cseq1.append(c1)
//  cseq1.append(c2)
//  cseq1.append(c3)
//  cseq1.append(c7)
//
//  println("Cseq 1")
//  println(cseq1)
//
//  val cseq2: Cstruct = new Cseq()
//
//  cseq2.append(c1)
//  cseq2.append(c2)
//  cseq2.append(c3)
//  cseq2.append(c4)
//
//  println("Cseq 2")
//  println(cseq2)
//
//  println("GLB between cseq1 and cseq2")
//  val derpz = cseq1.glb(Set(cseq2))
//  println(derpz)
//
//  println("LUB between cseq1 and cseq2")
//  val derpzz = cseq1.lub(Set(cseq2))
//  println(derpzz)


//  val cseq_test1: Cstruct = new Cseq()
//  val cseq_test2: Cstruct = new Cseq()
//  val cseq_test3: Cstruct = new Cseq()
//  val r = scala.util.Random
//
//  var i = 0
//  for(i <- 1 to 500){
//    cseq_test1.append(new SimpleCommandInt(r.nextInt(10)))
//    cseq_test2.append(new SimpleCommandInt(r.nextInt(10)))
//    cseq_test3.append(new SimpleCommandInt(r.nextInt(10)))
//  }
//
//  val cseq_tests = Set(cseq_test2, cseq_test3)
//
//  val cseq_lub_result: Cstruct = cseq_test1.lub(cseq_tests)
//  val cseq_glb_result: Cstruct = cseq_test1.glb(cseq_tests)
//  println(cseq_glb_result)
//  println(cseq_lub_result)

//  val cset_test1: Cstruct = new Cset()
//  val cset_test2: Cstruct = new Cset()
//  val cset_test3: Cstruct = new Cset()
//  val r2 = scala.util.Random
//	
//
//

//	var seti: LinkedHashSet[Command] = LinkedHashSet()
//
//	var cmd1: Command = new SimpleCommandInt(3)
//	val cmd2: Command = new SimpleCommandInt(7)
//	val cmd3: Command = new SimpleCommandInt(1)
//	val cmd4: Command = new SimpleCommandInt(0)
//	val cmd5: Command = new SimpleCommandInt(1)
//	val cmd6: Command = new SimpleCommandInt(5)
//	val cmd7: Command = new SimpleCommandInt(1)
//
//	seti = seti + cmd1
//	println(seti)
//	seti = seti + cmd2
//	println(seti)
//	seti = seti + cmd3
//	println(seti)
//	seti = seti + cmd4
//	println(seti)
//	seti = seti + cmd5
//	println(seti)
//	seti = seti + cmd6
//	println(seti)
//	seti = seti + cmd7
//	println(seti)

//	var setii = seti
//	println(setii == seti)
//
//	cmd1 = new SimpleCommandInt(666)
//	seti = seti + cmd1
//
//	println(seti)
//	println(setii)


//
//  var j = 0
//  for(j <- 1 to 10){
//    cset_test1.append(new SimpleCommandInt(r2.nextInt(10)))
//    cset_test2.append(new SimpleCommandInt(r2.nextInt(10)))
//    cset_test3.append(new SimpleCommandInt(r2.nextInt(10)))
//  }
//
//  println(cset_test1)
//
//  println(cset_test2)
//
//  println(cset_test3)
//
//  val cset_tests = Set(cset_test2, cset_test3)
//  println(cset_tests)
//
//  //val cseq_glb_result = cseq_glb_test.glb(cseq_tests)
//
//  val cset_lub_result: Cstruct = cset_test1.lub(cset_tests)
//  val cset_glb_result: Cstruct = cset_test1.glb(cset_tests)
//
//  val csetx: Cset = cset_glb_result.asInstanceOf[Cset]
//  val csetxx: Cset = cset_lub_result.asInstanceOf[Cset]
//
//  println(csetxx.value.size)
//  println(csetx.value.size)

