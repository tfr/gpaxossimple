# How to run this code

### Run sbt

Please look in the official [documentation of sbt](http://www.scala-sbt.org/release/docs/Getting-Started/Running.html) for more commands.

```
Running with SBT:
sbt -Dconfig.file=benchmark/debug/mcgp-debug.conf 'run-main Main node1'

```
